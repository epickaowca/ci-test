Cypress.Commands.add("getBySel", (selector, ...args) => {
  return cy.get(`[data-cy=${selector}]`, ...args);
});

declare namespace Cypress {
  interface Chainable {
    findBySel(selector: string, ...args): Chainable<JQuery<HTMLElement>>;
    getBySel(selector: string, ...args): Chainable<JQuery<HTMLElement>>;
  }
}
