/// <reference types="cypress" />

// const todosTitlesMoock = [
//   {
//     title: "test1",
//     id: 1,
//   },
//   {
//     title: "test2",
//     id: 2,
//   },
//   {
//     title: "test3",
//     id: 3,
//   },
// ];

// describe("test", () => {
//   it("stub test", () => {
//     cy.intercept("GET", "https://jsonplaceholder.typicode.com/todos?_limit=5", {
//       statusCode: 200,
//       body: todosTitlesMoock,
//     }).as("getTodos");

//     cy.visit("http://localhost:3000/");

//     cy.wait("@getTodos");

//     cy.getBySel("list_item")
//       .should("have.length", 3)
//       .each(($todo, index) => {
//         cy.wrap($todo).should("contain.text", todosTitlesMoock[index].title);
//       });
//   });
// });
