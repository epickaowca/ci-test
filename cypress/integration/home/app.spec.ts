/// <reference types="cypress" />

//comment

import { correctPass } from "../../../src/redux/duck/loginApp";
import { logIn } from "../../../src/redux/duck/loginApp";

describe("login tests", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/");
  });

  it("should give warning communicate when password incorrect", () => {
    //todo wrapper should not be in document
    cy.getBySel("todoWrapper").should("not.exist");

    //comunicat should be normal
    cy.getBySel("login_comunicat").should(
      "have.text",
      "enter username and password"
    );

    //submit form with bad pass
    cy.getBySel("username_input").type(`${correctPass.login}123{enter}`);
    cy.focused().type(`${correctPass.pass}123{enter}`);

    //todo wrapper should not be in document
    cy.getBySel("todoWrapper").should("not.exist");

    //comunicat should be error
    cy.getBySel("login_comunicat").should(
      "have.text",
      "username or password incorrect!"
    );
  });
  it("should login correct when password is correct", () => {
    //todo wrapper should not be in document
    cy.getBySel("todoWrapper").should("not.exist");

    //submit form with bad pass
    cy.getBySel("username_input").should("be.visible").type(correctPass.login);
    cy.getBySel("password_input")
      .should("be.visible")
      .type(`${correctPass.pass}{enter}`);

    //todo wrapper should be visible now
    cy.getBySel("todoWrapper").should("be.visible");
  });
});

describe("todo app tests", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/");

    //dispatch login action for skip ui login
    cy.window()
      .its("__store__")
      .then((store) =>
        store.dispatch(
          logIn({ login: correctPass.login, pass: correctPass.pass })
        )
      );
  });

  it("todos section should render and work properly ", () => {
    //header correct render
    cy.getBySel("todos_header").should("be.visible").and("have.text", "Todos:");

    //second todo item correct value
    cy.getBySel("todo_item")
      .last()
      .should("be.visible")
      .find("[data-cy=todo_content]")
      .should("have.value", "buy milk");

    //first todo item correct value
    //first todo delete button correct background-color
    //able to click delete button
    cy.getBySel("todo_item")
      .first()
      .should("be.visible")
      .find("[data-cy=todo_content]")
      .should("have.value", "go for a walk")
      .parent()
      .find("[data-cy=todo_delete_btn]")
      .should("have.css", "background-color", "rgb(255, 0, 0)")
      .click();

    //only one todo remained
    //able to click delete button
    cy.getBySel("todo_item")
      .should("have.length", "1")
      .find("[data-cy=todo_content]")
      .should("have.value", "buy milk")
      .parent()
      .find("[data-cy=todo_delete_btn]")
      .click();

    //no todos anymore
    cy.getBySel("todo_item").should("have.length", "0");

    //todos contaier still visible
    cy.getBySel("todos_container").should("be.visible");
  });

  it("add new todo should shows error message if content is empty", () => {
    //title should be visible and have text
    cy.getBySel("add_todo_title")
      .should("be.visible")
      .and("have.text", "add new todo:");

    //error message should not be in document
    cy.getBySel("add_todo_error_message").should("not.exist");

    //input should not have value at start
    cy.getBySel("add_todo_input_content")
      .should("be.visible")
      .and("not.have.value");

    //button should have correct color and be able to click
    cy.getBySel("add_todo_submit_btn")
      .should("be.visible")
      .and("have.css", "background-color", "rgb(54, 222, 121)")
      .click();

    //shows error message after click button without fill input
    cy.getBySel("add_todo_error_message")
      .should("be.visible")
      .and("have.text", "fill in the field!");
  });

  it("add new todo should work properly", () => {
    const newTodoContent = "wash the dishes";
    //fill input with todo content
    cy.getBySel("add_todo_input_content")
      .should("be.visible")
      .type(newTodoContent);

    //able to click submit btn
    cy.getBySel("add_todo_submit_btn").should("be.visible").click();

    //error message should not popup
    cy.getBySel("add_todo_error_message").should("not.exist");

    //new todoItem added correctly
    cy.getBySel("todo_item")
      .should("have.length", 3)
      .last()
      .should("be.visible")
      .find("[data-cy=todo_content]")
      .and("have.value", newTodoContent);
  });

  it("should logout correctly", () => {
    //button correct render and able to click
    cy.getBySel("log_out_button")
      .should("be.visible")
      .and("have.text", "logout")
      .and("have.css", "background-color", "rgb(112, 112, 112)")
      .click();

    //todoWrapper should not be in document
    cy.getBySel("todoWrapper").should("not.exist");
  });
});

describe("todo added after second", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/");

    //dispatch login action for skip ui login
    cy.window()
      .its("__store__")
      .then((store) =>
        store.dispatch(
          logIn({ login: correctPass.login, pass: correctPass.pass })
        )
      );
    cy.getBySel("add_todo_input_content").type("nowe todo");
    cy.getBySel("add_todo_submit_btn").click();
  });

  it("passed test", () => {
    cy.getBySel("todo_content")
      .should("have.length", 3)
      .last()
      .should("have.value", "nowe todo");
  });
});
