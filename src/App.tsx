import { FC } from "react";
import Login from "./components/organisms/Login/Login";
import { useSelector } from "react-redux";
import { AppState } from "./redux/store";
import TodoWrapper from "./components/organisms/TodoWrapper/TodoWrapper";
import { QueryClient, QueryClientProvider } from "react-query";
import FetchedTodo from "./components/organisms/FetchedTodo.tsx/FetchedTodo";
import styled from "styled-components";

const StyledDiv = styled.div`
  display: none;
`;

const queryClient = new QueryClient();

const App: FC = () => {
  const login = useSelector((app: AppState) => app.loginApp.login);

  return (
    <>
      <div>{login ? <TodoWrapper /> : <Login />}</div>
      {/* <QueryClientProvider client={queryClient}>
        <FetchedTodo />
      </QueryClientProvider> */}
      {/* <div>
        <ul className="ul_class">
          <li>milk</li>
          <li>cow</li>
          <li>milk</li>
          <li>chees</li>
          <li>chees</li>
          <a href="coos">xd</a>
        </ul>
        <StyledDiv data-cy="divek">This is div</StyledDiv>
      </div> */}
    </>
  );
};

export default App;
