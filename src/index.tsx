import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { myTheme } from "./themeProvider/themeProvider";
import { ThemeProvider } from "styled-components";
import store from "./redux/store";
import { Provider } from "react-redux";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ThemeProvider theme={myTheme}>
        <App />
      </ThemeProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// @ts-ignore
if (window.Cypress) {
  // @ts-ignore
  window.__store__ = store;
}
