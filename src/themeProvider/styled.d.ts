import "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    colors: {
      green: "#36DE79";
      gray: "#707070";
      red: "#FF0000";
    };
  }
}
