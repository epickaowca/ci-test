import { DefaultTheme } from "styled-components";

const myTheme: DefaultTheme = {
  colors: {
    green: "#36DE79",
    gray: "#707070",
    red: "#FF0000",
  },
};

export { myTheme };
