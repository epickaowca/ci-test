import { createStore } from "redux";
import reducer from "./duck";

export type AppState = ReturnType<typeof reducer>;

const store = createStore(reducer);

export default store;
