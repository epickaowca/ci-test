import { combineReducers } from "redux";
import loginApp from "./loginApp";
import todoApp from "./todoApp";

const rootReducer = combineReducers({ loginApp, todoApp });

export default rootReducer;
