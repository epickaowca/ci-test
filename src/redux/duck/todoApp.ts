import nextId from "react-id-generator";
export const ADD_TODO = "loginApp/add_todo";
export const REMOVE_TODO = "loginApp/remove_todo";

export interface loginStateInterface {
  readonly todos: { title: string; id: string }[];
}

const initialState: loginStateInterface = {
  todos: [
    { title: "go for a walk", id: nextId() },
    { title: "buy milk", id: nextId() },
  ],
};

const reducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case ADD_TODO: {
      return {
        ...state,
        todos: [...state.todos, { title: action.title, id: nextId() }],
      };
    }
    case REMOVE_TODO: {
      return {
        ...state,
        todos: state.todos.filter((item) =>
          item.id === action.id ? null : item
        ),
      };
    }
    default:
      return state;
  }
};

export type AddTodoType = { type: typeof ADD_TODO; title: string };
export type RemoveTodoType = { type: typeof REMOVE_TODO; id: string };

export const add_todo = (title: string): ActionTypes => ({
  type: ADD_TODO,
  title,
});

export const remove_todo = (id: string): ActionTypes => ({
  type: REMOVE_TODO,
  id,
});

export type ActionTypes = AddTodoType | RemoveTodoType;

export default reducer;
