export const LOG_IN = "loginApp/log_in";
export const LOG_OUT = "loginApp/log_out";

export interface loginStateInterface {
  readonly login: boolean;
  readonly badPass: boolean;
}

const initialState: loginStateInterface = {
  login: false,
  badPass: false,
};

export const correctPass = {
  login: "desktop",
  pass: "admin",
};

const reducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case LOG_IN: {
      const { login: loginC, pass: passC } = correctPass;
      const { login, pass } = action.payload;

      if (loginC === login && passC === pass) {
        return {
          ...state,
          login: true,
          badPass: false,
        };
      } else {
        return {
          ...state,
          login: false,
          badPass: true,
        };
      }
    }
    case LOG_OUT:
      return {
        ...state,
        login: false,
        badPass: false,
      };
    default:
      return state;
  }
};

type loginPayloadType = { login: string; pass: string };

export type LogInType = { type: typeof LOG_IN; payload: loginPayloadType };
export type LogOutType = { type: typeof LOG_OUT };

export const logIn = (payload: loginPayloadType): ActionTypes => ({
  type: LOG_IN,
  payload,
});
export const logOut = (): ActionTypes => ({ type: LOG_OUT });

export type ActionTypes = LogInType | LogOutType;

export default reducer;
