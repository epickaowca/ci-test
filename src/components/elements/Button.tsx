import { FC } from "react";
import styled from "styled-components";

const StyledBtn = styled.button<StyledBtnInterface>`
  --font-size-V: 1rem;
  --padding-V: 10px 20px;
  border: none;
  background-color: ${(p) => p.theme.colors[p.color]};
  color: white;
  font-size: var(--font-size-V);
  padding: var(--padding-V);
  font-weight: 600;
  cursor: pointer;
  ${(p) => p.large && "--font-size-V: 2rem; --padding-V: 20px 45px;"}
  &:hover {
    opacity: 0.7;
  }
`;

interface StyledBtnInterface {
  color: "green" | "gray" | "red";
  large?: boolean;
}

interface BtnInterface extends StyledBtnInterface {
  content: string;
  onClick?: (id?: any) => any;
}

const Button: FC<BtnInterface> = ({ content, ...rest }) => {
  return <StyledBtn {...rest}>{content}</StyledBtn>;
};

export default Button;
