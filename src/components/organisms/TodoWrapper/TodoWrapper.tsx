import { FC } from "react";
import styled from "styled-components";
import TodosContainer from "./elements/TodosContainer";
import AddTodo from "./elements/AddTodo";
import Button from "../../elements/Button";
import { useDispatch } from "react-redux";
import { logOut } from "../../../redux/duck/loginApp";

const StyledTodoWrapper = styled.div`
  background: white;
  position: absolute;
  width: 1000px;
  height: 600px;
  left: 50%;
  top: 20%;
  transform: translateX(-50%);
  padding: 55px;
  display: flex;
  justify-content: space-between;
  & > div {
    display: flex;
    flex-direction: column;
    &:nth-child(1) {
      gap: 45px;
    }
    &:nth-child(2) {
      justify-content: space-between;
    }
  }
`;

const TodoWrapper: FC = () => {
  const dispatch = useDispatch();

  const logOutFunc = () => {
    dispatch(logOut());
  };

  return (
    <StyledTodoWrapper data-cy="todoWrapper">
      <div>
        <h1 data-cy="todos_header">Todos:</h1>
        <TodosContainer />
      </div>
      <div>
        <AddTodo />
        <Button
          data-cy="log_out_button"
          large
          color="gray"
          content="logout"
          onClick={logOutFunc}
        />
      </div>
    </StyledTodoWrapper>
  );
};

export default TodoWrapper;
