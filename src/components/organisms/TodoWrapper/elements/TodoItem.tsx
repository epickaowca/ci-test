import { FC } from "react";
import Button from "../../../elements/Button";
import styled from "styled-components";

const StyledItem = styled.div`
  border: 1px solid ${(p) => p.theme.colors.gray};
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 15px 25px;
  width: 450px;
  & > input {
    width: 280px;
    background-color: transparent;
    border: none;
    overflow-hidden;
    outline: none;
  }
`;

interface TodoItemInterface {
  remove: () => {};
  todoContent: string;
}

const TodoItem: FC<TodoItemInterface> = ({ todoContent, remove }) => {
  return (
    <StyledItem data-cy="todo_item">
      <input data-cy="todo_content" type="text" value={todoContent} readOnly />
      <Button
        data-cy="todo_delete_btn"
        content="delete"
        color="red"
        onClick={remove}
      />
    </StyledItem>
  );
};

export default TodoItem;
