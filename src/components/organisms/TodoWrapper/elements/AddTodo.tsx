import { FC, useState } from "react";
import Button from "../../../elements/Button";
import { add_todo } from "../../../../redux/duck/todoApp";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { inputStyle } from "../../Login/elements/Form";

const StyledAddTodo = styled.div`
  &,
  & > form {
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    gap: 15px;
  }
  & > form {
    margin-top: 25px;
    & > input {
      ${inputStyle}
    }
  }
  & > p {
    color: ${(p) => p.theme.colors.red};
  }
`;

const ButtonsContainer: FC = () => {
  const [todoContent, setTodoContent] = useState("");
  const [error, setError] = useState(false);
  const dispatch = useDispatch();

  const Submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const content = todoContent.replace(/\s/g, "");
    if (content) {
      setTimeout(() => {
        dispatch(add_todo(todoContent));
      }, 1000);
      setError(false);
      setTodoContent("");
    } else {
      setError(true);
    }
  };

  return (
    <StyledAddTodo>
      <h2 data-cy="add_todo_title">add new todo:</h2>
      {error && <p data-cy="add_todo_error_message">fill in the field!</p>}
      <form onSubmit={Submit}>
        <input
          data-cy="add_todo_input_content"
          value={todoContent}
          onChange={(e) => setTodoContent(e.target.value)}
          type="text"
          placeholder="todo content"
        />
        <Button
          data-cy="add_todo_submit_btn"
          content="add todo"
          color="green"
        />
      </form>
    </StyledAddTodo>
  );
};

export default ButtonsContainer;
