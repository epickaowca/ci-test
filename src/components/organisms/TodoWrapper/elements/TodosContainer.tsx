import { FC, useEffect } from "react";
import TodoItem from "./TodoItem";
import { useDispatch, useSelector } from "react-redux";
import { remove_todo } from "../../../../redux/duck/todoApp";
import { AppState } from "../../../../redux/store";
import styled from "styled-components";

const StyledTodosContainer = styled.div`
  width: 500px;
  height: 450px;
  border: 1px solid ${(p) => p.theme.colors.gray};
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px 0px;
  gap: 25px;
  overflow-y: auto;
`;

const TodosContainer: FC = () => {
  const todoList = useSelector((app: AppState) => app.todoApp.todos);
  const dispatch = useDispatch();
  useEffect(() => {}, []);
  return (
    <StyledTodosContainer data-cy="todos_container">
      {todoList.map(({ id, title }) => (
        <TodoItem
          key={id}
          todoContent={title}
          remove={() => dispatch(remove_todo(id))}
        />
      ))}
    </StyledTodosContainer>
  );
};

export default TodosContainer;
