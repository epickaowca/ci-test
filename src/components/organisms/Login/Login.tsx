import { FC } from "react";
import Form from "./elements/Form";
import Comunicat from "./elements/Comunicat";
import styled from "styled-components";

const StyledLogin = styled.div`
  background: white;
  display: flex;
  flex-direction: column;
  position: absolute;
  width: 600px;
  height: 400px;
  left: 50%;
  top: 20%;
  transform: translateX(-50%);
  padding: 25px;
`;

const Login: FC = () => {
  return (
    <StyledLogin>
      <Comunicat />
      <Form />
    </StyledLogin>
  );
};

export default Login;
