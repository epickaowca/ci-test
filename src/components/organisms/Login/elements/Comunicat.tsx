import { FC } from "react";
import styled from "styled-components";
import { useSelector } from "react-redux";
import { AppState } from "../../../../redux/store";

const comunicats = {
  normal: "enter username and password",
  error: "username or password incorrect!",
};

type StyledP = {
  error: boolean;
};

const StyledP = styled.p<StyledP>`
  color: ${(p) => p.theme.colors[p.error ? "red" : "gray"]};
`;

const Comunicat: FC = () => {
  const badPass = useSelector((app: AppState) => app.loginApp.badPass);
  const content = badPass ? comunicats.error : comunicats.normal;
  return (
    <StyledP data-cy="login_comunicat" error={badPass}>
      {content}
    </StyledP>
  );
};

export default Comunicat;
