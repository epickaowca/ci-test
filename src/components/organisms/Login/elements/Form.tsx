import { FC, useState } from "react";
import Button from "../../../elements/Button";
import styled, { css } from "styled-components";
import { useDispatch } from "react-redux";
import { logIn } from "../../../../redux/duck/loginApp";

export const inputStyle = css`
  width: 250px;
  padding: 15px 10px;
  border-radius: 0px;
  outline: none;
  border: 1px solid ${(p) => p.theme.colors.gray};
`;

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  margin: auto;
  gap: 25px;
  & > input {
    ${inputStyle};
  }
`;

const Form: FC = () => {
  const [username, setUsername] = useState("");
  const [pass, setPass] = useState("");
  const dispatch = useDispatch();
  const Submit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    dispatch(logIn({ login: username, pass: pass }));
    setUsername("");
    setPass("");
  };

  return (
    <StyledForm action="#" onSubmit={Submit}>
      <input
        value={username}
        onChange={(e) => setUsername(e.target.value)}
        type="text"
        placeholder="username"
        required
        data-cy="username_input"
      />
      <input
        value={pass}
        onChange={(e) => setPass(e.target.value)}
        type="password"
        placeholder="password"
        required
        data-cy="password_input"
      />
      <Button content="submit" color="green" />
    </StyledForm>
  );
};

export default Form;
