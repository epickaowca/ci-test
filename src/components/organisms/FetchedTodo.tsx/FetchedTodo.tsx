import { FC } from "react";
import { useQuery } from "react-query";

const fetchTodos = async () => {
  const res = fetch("https://jsonplaceholder.typicode.com/todos?_limit=5").then(
    (res) => res.json()
  );
  return res;
};

const FetchedTodo: FC = () => {
  const { isLoading, error, data } = useQuery("repoData", () => fetchTodos());

  if (isLoading) return <h1>"Loading..."</h1>;
  if (error) return <h1>"An error has occurred "</h1>;

  return (
    <ul>
      <input type="checkbox" style={{ margin: "50px" }} />
      {data.map((elem: any) => (
        <li data-cy="list_item" key={elem.id}>
          {elem.title}
        </li>
      ))}
    </ul>
  );
};

export default FetchedTodo;
